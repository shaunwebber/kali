kali
----
kali-linux container image built monthly with a collection of pentesting tools ready to go.

pull::

    sudo podman pull registry.gitlab.com/snotra.uk/containers/kali

run::

    sudo podman run \
        -ti \
        --security-opt label=disable `#required for host file access if se linux is enabeld`\
        --rm --network host `#required for catching shells etc`\
        --privileged `#required for low level network access`\
        -v $PWD:/run/host `#update with your desired mounts`\
        registry.gitlab.com/snotra.uk/containers/kali

if you are using docker just replace podman with docker.
